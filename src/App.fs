module App.View

open Prelude
open Elmish
open Fable.Core.JsInterop
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome
open Model
open Update

let private view model dispatch =
    let currentPage = getPage model.CurrentPage
    let pageComplete = Set.contains model.CurrentPage model.Completed
    Container.container []
        [ Columns.columns []
              [ Column.column [ Column.Width(Fulma.Screen.All, Column.Is3) ]
                    (Sidebar.view model dispatch)

                Column.column []
                    (PageView.view currentPage pageComplete dispatch) ]

          Footer.footer []
              [ Content.content
                    [ Content.Modifiers
                          [ Modifier.TextAlignment
                                (Screen.All, TextAlignment.Centered) ] ]
                    [ str "A joint production brought to you by "

                      a [ Href "https://noredink.com" ]
                          [ img [ Style [ Height "1em" ]
                                  Src "assets/nri.svg"
                                  Alt "NoRedInk" ] ]
                      str " and "
                      a [ Href "https://blog.mavnn.co.uk" ] [ str "mavnn" ]
                      str " with many thanks to the "

                      a [ Href "https://twitter.com/FableCompiler" ]
                          [ str "Fable community" ]
                      str ", especially "

                      a [ Href "https://twitter.com/alfonsogcnunez" ]
                          [ str "Alfonso" ]
                      str " and "

                      a [ Href "https://twitter.com/MangelMaxime" ]
                          [ str "Maxime" ] ] ] ]

open Elmish.React
open Elmish.Debug
open Elmish.HMR

Program.mkProgram init storingUpdate view
#if DEBUG
|> Program.withHMR
#endif

|> Program.withReactUnoptimized "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif

|> Program.run
