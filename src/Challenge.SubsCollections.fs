module Challenge.SubsCollections

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """Sometimes you don't just want a specific number of subcomponents.
         Maybe you don't know how many pieces of data you're loading. Maybe
         the user should be able to add and remove copies."""
      p_ """Let's have a think how to have a collection of subcomponents.
         There's multiple parts to this challenge, as you'll need to make
         additions to the parent application at all levels, including adding
         some parent controls."""
      p_ """Replace "Model", "Msg" and "init" in the parent application as
         below, and see if you can build an application where the user can
         add and remove independent Pokemon displays."""
      fsharpBlock """
type Model =
{ Pokemons : Map<int, Pokemon.Model> }

type Msg =
    | UpdatePokemon of int * Pokemon.Msg
    | AddPokemon
    | RemovePokemon of int

let init _ =
    let model = { Pokemons = Map.empty }
    model, Cmd.none
      """ ]

let page =
    { Id = ChallengeSubsCollections
      Title = "Creating Collections of Subcomponents"
      Subtitle = Some "Gotta collect 'em all!"
      Content = content
      Requires = Set.ofList [ ChallengeMultipleSubs ] }
