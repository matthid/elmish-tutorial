module Extras.Subcomponents

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """The Elmish app we've created has a nice flow to it;
         "msg" -> "update" -> "view" back to "msg". But it might
         not be clear how to scale it."""
      p_ """Fortunately, it turns out this is fairly simple. Any
         Elmish style application (i.e., the combination of "model"
         , "messages", "update" and "view") can be turned into a
         subcomponent of a larger Elmish application."""
      p_ """Let's walk through how to do that!"""
      p_ """Firstly, create a new file in your F# project called
         "Pokemon.fs". Files
         in F# are compiled in order, so make sure it comes above
         "App.fs" in your project."""
      p_ """Copy everything currently in "App.fs" to "Pokemon.fs"
         apart from the actual running of the JavaScript
         ("open Elmish.React") and down. Change the module name
         at the top of the file "Pokemon" and remove the "private"
         keyword anywhere it appears."""
      p_ """We now have a subcomponent - our Pokemon display.
         Now we need to build a parent application to host it.
         Back in "App.fs", we start with deleting the Pokemon type
         and replacing the model:"""
      fsharpBlock """
type Model =
    { PokemonDisplayModel : Pokemon.Model }
"""
      p_ """Our host application has no state of it's own (yet),
         so it just stores the state of it's subcomponent in a field.
         Now, what update our update logic?"""
      p_ """Delete the fetchPokemon function - that's now the
         responsibility of the Pokemon module. Then replace our
         update logic like this:"""
      fsharpBlock """
type Msg =
    | UpdatePokemonDisplay of Pokemon.Msg

let init _ =
    let pokemonModel, pokemonCmds =
        Pokemon.init ()
    { PokemonDisplayModel = pokemonModel },
    Cmd.map UpdatePokemonDisplay pokemonCmds

let update msg model =
    match msg with
    | UpdatePokemonDisplay pmsg ->
        let pokemonModel, pokemonCmds =
            Pokemon.update pmsg model.PokemonDisplayModel
        { model with PokemonDisplayModel = pokemonModel },
        Cmd.map UpdatePokemonDisplay pokemonCmds
"""
      p_ """What's going on here? We've created a wrapper "Msg" in our
         top level application which knows to direct "Pokemon.Msg"
         messages to the Pokemon subcomponent. Then in update, we
         delegate those messages to the update function of the Pokemon
         module. The quirk here is that any commands issued by the
         subcomponent will be messages for the subcomponent, not the
         parent application. To make sure they're of the correct type
         and that we route them back to the right place, we use "Cmd.map"
         to wrap them up in the UpdatePokemonDisplay message of the
         parent."""
      p_ """Finally, we need to apply a similar strategy to our view:
         we delegate the production of the view to the Pokemon
         module, and make sure we wrap any messages produced to
         route them to the correct place."""
      fsharpBlock """
let view model dispatch =
    let pokemonView =
        Pokemon.view model.PokemonDisplayModel
            (fun m -> dispatch (UpdatePokemonDisplay m))
    Container.container [] [ pokemonView ]
"""
      p_ """Nearly done. The last change is cosmetic: generally, an
         entire Fulma app sits inside a single "Container.container"
         so we've
         added that to the parent applications view. Take it out of
         the view in the Pokemon module, and
         replace it with a "Box.box'".""" ]

let page =
    { Id = ExtrasSubcomponents
      Title = "Building Subcomponents"
      Subtitle = Some "Or how to make Lego blocks"
      Content = content
      Requires = Set.ofList [ BeginningsDispatch ] }
