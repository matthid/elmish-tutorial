module Beginnings.Dispatch

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """Loading data on page load is lovely, but often we want
         to do things in response to the user as well. Let's add a
         Pokemon ID selector and a "Load" button to our Pokemon
         display."""
      p_ """First, we're going to need to keep track of some extra
         state. Update the model type and add a field to it called
         "PokemonId" with a type of "int"."""
      p_ """Now, our update functionality. We're going to have plus
         and minus buttons on the ID selector, so we need increment
         and decrement ID messages in our message type. We'll also
         need to change the "update" function to respond to them; it
         will end up with a few changed/extra lines like this:"""
      fsharpBlock """
let update msg model =
    // ...
    | LoadPokemon ->
        { model with Loading = true
                     PokemonData = None },
        Cmd.ofPromise fetchPokemon model.PokemonId PokemonLoaded
            (fun e -> LoadingFailed e.Message)
    | IncreasePokemonId ->
        { model with PokemonId = model.PokemonId + 1 }, Cmd.none
    | DecreasePokemonId ->
        { model with PokemonId = model.PokemonId - 1 }, Cmd.none
"""
      p_ """(Remember to update your "Msg" type with the new messages too!)"""
      p_ """Most importantly, we need to update our view. We'll add a function
         to create plus, minus and load buttons:"""

      fsharpBlock """
let idSelector model dispatch =
    [ Level.level []
          [ Level.item [ Level.Item.HasTextCentered ]
                [ Button.button
                      [ Button.Color IsDanger

                        Button.Props
                            [ OnClick(fun _ -> dispatch DecreasePokemonId) ] ]
                      [ str "-" ] ]

            Level.item [ Level.Item.HasTextCentered ]
                [ str (sprintf "%d" model.PokemonId) ]

            Level.item [ Level.Item.HasTextCentered ]
                [ Button.button
                      [ Button.Color IsSuccess

                        Button.Props
                            [ OnClick(fun _ -> dispatch IncreasePokemonId) ] ]
                      [ str "+" ] ] ]

      Level.level []
          [ Level.item [ Level.Item.HasTextCentered ]
                [ Button.button
                      [ Button.IsActive(not model.Loading)
                        Button.Color IsPrimary
                        Button.Props [ OnClick(fun _ -> dispatch LoadPokemon) ] ]
                      [ str "Load Pokemon!" ] ] ] ]
"""
      p_ """Now you should be able to update the content line in your
         main view function to include the selector; it will end up
         looking something like this:"""
      fsharpBlock """
let view model dispatch =
    // ...
    Container.container []
        [ Content.content [] (List.append content (idSelector model dispatch)) ]
"""
      p_ """Notice how the various buttons call the dispatch function
         to cause updates to the applications state. And then Elmish
         takes the new state, and gives it back to the "view" function,
         starting the cycle all over again."""
      ]

let page =
    { Id = BeginningsDispatch
      Title = "Dispatching User Messages"
      Subtitle = Some "Click, click, clickerty click!"
      Content = content
      Requires = Set.ofList [ BeginningsLoad ] }
