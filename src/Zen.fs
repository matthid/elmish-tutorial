module Zen

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """Excellent work! You've completed all the challenges
         currently available."""
      Heading.h6 [] [ str """Time to go out, and take over the world!""" ] ]

let page =
    { Id = Zen
      Title = "Congratulations"
      Subtitle = None
      Content = content
      Requires = Set.remove Zen allPages }
