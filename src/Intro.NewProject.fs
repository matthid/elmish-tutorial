module Intro.NewProject

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome

let content =
    [ p_ """
      New it's time to create a new project for our Fable application.
      """
      p_ """Create a new directory, and navigate to it in the console.
      Then run the following commands to download the latest version
      of a dotnet template, and create the project."""
      noneBlock """# This command only needs to be run the first time:
dotnet new -i Fable.Template.Fulma.Minimal

# Run this for each new project:
dotnet new fulma-minimal -lang f#
      """
      p_ """We can now try the template project out by running:"""
      noneBlock """./fake.sh build -t Watch

# or './fake.bat build -t Watch' on Windows
"""

      p []
          [ str """Once the project has finished downloading all it's
      dependencies and building, you should be able to browse to
      the resulting website at """
            a [ Href "http://localhost:8080/" ] [ str "http://localhost:8080/" ] ]
      p_ """Have a quick play with the website -
      it will help understand what's going on.""" ]

let page =
    { Id = IntroNewProjectFromTemplate
      Title = "Create Our New Project"
      Subtitle = None
      Content = content
      Requires = Set.ofList [ IntroPrereqs ] }
