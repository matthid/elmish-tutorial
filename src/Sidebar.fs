module Sidebar

open Prelude
open Elmish
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fulma
open Fulma.FontAwesome
open Model
open SharedViews
open Update

let isAvailable model pageId =
    let page = getPage pageId
    List.forall (fun pid -> Set.contains pid model.Completed)
        (page.Requires |> Set.toList)

let pageLink model dispatch pageId =
    let page = getPage pageId
    let isActive = model.CurrentPage = page.Id
    let isComplete = Set.contains pageId model.Completed
    li []
        [ a [ Props.OnClick(fun _ -> dispatch (ChangePage pageId)) ]
              [ Text.span
                    [ if isActive then
                          yield Modifiers [ Modifier.TextColor IsPrimary ] ]
                    (checkedIfComplete page.Title isComplete) ] ]

let view model dispatch =
    [ Content.content [] [ Heading.h6 [] [ str "Pages:" ]
                           ul [] (allPages
                                  |> Set.toList
                                  |> List.filter (isAvailable model)
                                  |> List.map (pageLink model dispatch)) ] ]
    |> Section.section []
    |> List.singleton
