module Update

open Prelude
open Elmish
open Fable.Core.JsInterop
open Fable.Import.Browser
open Model

let storeModel model = window.localStorage.setItem ("model", toJson model)

let defaultModel =
    { Value = ""
      Completed = Set.empty
      CurrentPage = IntroPrereqs }

let getModel() =
    try
        let last = window.localStorage.getItem "model" :?> string option
        match last with
        | Some m ->
            let deserialized = ofJson<Model> m
            deserialized.CurrentPage |> ignore
            deserialized.Completed |> ignore
            deserialized.Value |> ignore
            deserialized
        | None -> defaultModel
    with _ -> defaultModel

let init _ = getModel(), Cmd.none

type Msg =
    | ChangeValue of string
    | ChangePage of PageId
    | CompletePage of PageId
    | ScrollUp
    | ClearProgress

let update msg model =
    match msg with
    | ChangeValue newValue -> { model with Value = newValue }, Cmd.none
    | ChangePage newPage ->
        { model with CurrentPage = newPage }, Cmd.ofMsg ScrollUp
    | CompletePage pageId ->
        { model with Completed = Set.add pageId model.Completed },
        Cmd.ofMsg ScrollUp
    | ScrollUp ->
        window.scroll (0., 0.)
        model, Cmd.none
    | ClearProgress -> defaultModel, Cmd.ofMsg ScrollUp

let storingUpdate msg model =
    let (newModel, cmds) = update msg model
    storeModel newModel
    newModel, cmds
